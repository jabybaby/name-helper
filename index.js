let express = require('express');
let app = express();
let fs = require("fs");
let done = require("./done.json");
let names = require("./names.json")

app.set('view engine', 'ejs');
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.get('/', (req, res) => {
  res.render('index', {count: names.filter(el => !done.map(el2 => el2.name).some(el2 => el2 == el)).length, name: shuffle(names.filter(el => !done.map(el2 => el2.name).some(el2 => el2 == el)))[0]});
});

app.post('/name', (req, res) => {
    done.push({name: req.body.name, female: req.body.female == "like", male: req.body.male == "like"})
    fs.writeFile("done.json", JSON.stringify(done), () => {});
    res.redirect("/");
});

app.post('/nem', (req, res) => {
    done.push({name: req.body.name, female:false, male: false})
    fs.writeFile("done.json", JSON.stringify(done), () => {});
    res.redirect("/");
});

app.listen(4000, () => console.log('Example app listening on port 4000!'));


function shuffle(array) {
  let currentIndex = array.length,  randomIndex;

  // While there remain elements to shuffle.
  while (currentIndex != 0) {

    // Pick a remaining element.
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex--;

    // And swap it with the current element.
    [array[currentIndex], array[randomIndex]] = [
      array[randomIndex], array[currentIndex]];
  }

  return array;
}
